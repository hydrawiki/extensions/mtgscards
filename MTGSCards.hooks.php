<?php
/**
 * MTGSCards
 * MTGSCards Hooks
 *
 * @license Creative Commons Attribution Share Alike 2.5 Generic
 * @author  Stephan Hoyer ("Darth Cow") - https://www.sccs.swarthmore.edu/users/08/hoyer/decktag/
 * @author  Further maintenance by the Hydra Wiki Platform Team for the MTGS Wiki.
 **/

class mtgsCardsHooks {
	/**
	 * Sets up tag hooks.
	 *
	 * @access public
	 * @return boolean	True
	 */
	public static function onParserFirstCallInit(Parser &$parser) {
		$parser->setHook("card", "mtgsCardsHooks::processCardTag");
		$parser->setHook("c", "mtgsCardsHooks::processCardTag");
		$parser->setHook("cards", "mtgsCardsHooks::processCardsTag");
		$parser->setHook("cs", "mtgsCardsHooks::processCardsTag");
		$parser->setHook("deck", "mtgsCardsHooks::processDeckTag");
		$parser->setHook("d", "mtgsCardsHooks::processDeckTag");

		return true;
	}

	public static function mtgsBbcodeDeckHandle($source, $id, $deck, $title, $pre = '', $post = '') {
		static $nums;
		if (!isset($nums[$source][$id])) {
			$nums[$source][$id] = 0;
		}
		$num = $nums[$source][$id]++;

		if ($title == '') {
			$title = 'Deck';
		}

		$deck = trim(strip_tags($deck));
		$deck = preg_replace("/(\r\n|\r|\n)/", "\n", $deck);
		$deck = preg_replace("/\n +\n/", "\n\n", $deck);
		$deck = preg_replace("/\n{2,}/", "\n\n", $deck);

		$finalcolumns = 1;
		$buf          = '<td>';
		$switch       = false;
		$i            = 0;
		$blocks       = explode("\n\n", $deck);
		$count        = substr_count($deck, "\n") - 2 * substr_count($deck, "\n\n");
		if (preg_match("/^(side board|sideboard|sb)/i", $blocks[count($blocks) - 1])) {
			$count -= substr_count($blocks[count($blocks) - 1], "\n");
		}
		foreach ($blocks as $block) {
			if (preg_match("/^(side board|sideboard|sb)/i", $block)) {
				$buf .= '</td><td class="sideboard">';
				$finalcolumns += 1;
			} elseif ($switch == false && $i > $count / 2) {
				$switch = true;
				$buf .= '</td><td>';
				$finalcolumns += 1;
			} elseif ($i > 0) {
				$buf .= '<br>';
			}
			$buf .= self::mtgsBbcodeCardsHandle($block, $pre, $post, true);
			$i += substr_count($block, "\n");
		}
		$buf .= '</td>';

		$cols = 1;
		if ($switch) {
			++$cols;
		}
		if (preg_match("/^(side board|sideboard|sb)/i", $blocks[count($blocks) - 1])) {
			++$cols;
		}

		return "<table class=\"deck\" border=1>"
			. "<tr><th colspan=\"" . $finalcolumns . "\">" . "<div class=\"download\">"
				. "<a href=\"/Special:DownloadDeck?source={$source}&id={$id}&num={$num}&format=apprentice\" title=\"Apprentice\"><img src=\"/extensions/MTGSCards/img/appr.gif\" style=\"border: 0px;\" /></a> "
				. "<a href=\"/Special:DownloadDeck?source=$source&id=$id&num=$num&format=octgn\" title=\"OCTGN2\"><img src=\"/extensions/MTGSCards/img/octgn.gif\" style=\"border: 0px;\" /></a> "
				. "<a href=\"/Special:DownloadDeck?source=$source&id=$id&num=$num&format=magiconline\" title=\"Magic Online\"><img src=\"/extensions/MTGSCards/img/modo.gif\" style=\"border: 0px; background: #000;\" /></a> "
			. "</div>" . "<b>" . $title . "</b> &nbsp;</th></tr>" . '<tr>' . $buf . '</tr></table>';
	}

	public static function mtgsBbcodeCardsHandle($cards, $pre = '', $post = '', $deck = false) {
		$lines = explode("\n", $cards);
		foreach ($lines as $line) {
			$line = trim($line);
			if (preg_match('/^([0-9]+x?\*? ?' . ($deck ? '' : '|') . ')(.*?)(| (\-(.*)|(\(|\[)(.*)(\)|\])))$/i', $line, $out)) {
				$buf .= sprintf('%s%s%s<br>', $out[1], self::mtgsBbcodeCardHandle($out[2], $out[2], $pre, $post), $out[3]);
			} else {
				$buf .= '<b>' . $line . '</b><br>';
			}
		}
		return $buf;
	}

	public static function mtgsBbcodeCardHandle($card, $title = '', $pre = '', $post = '') {
		global $thread, $foruminfo;

		$func = 'AutoCard';
		if ($thread['forumid'] == 28 or $foruminfo['forumid'] == 28) {
			$func = 'AutoCardGatherer';
		}

		if ($title != '') {
			list($title, $card) = [
				$card,
				$title
			];
		} else {
			$title = $card;
		}

		list($cardname, $edition) = explode('|', $card, 2);
		$cardname = str_replace("'", '%27', $cardname);
		if ($edition != '') {
			$edition = "', '" . str_replace("'", '%27', $edition);
		}

		if ($edition == '') {
			return '<a class="autocardhref" href="http://www.magiccards.info/query?q=%21' . urlencode($card) . '" onclick="' . $func . '(\'' . $cardname . $edition . '\'); return false;" title="' . $title . '"' . $pre . '>' . $title . '</a>' . $post;
		} else {
			return '<a class="autocardhref" href="http://www.magiccards.info/autocard/' . $card . '" onclick="' . $func . '(\'' . $cardname . $edition . '\'); return false;" title="' . $title . '"' . $pre . '>' . $title . '</a>' . $post;
		}
	}

	/**
	 * Processes Card Tags
	 *
	 * @access public
	 * @param  string	Content between opening and closing tags.
	 * @param  array	Arguments on the opening tag.
	 * @param  object	Mediawiki Parser
	 * @param  object	Mediawiki PPFrame
	 * @return string	HTML
	 */
	public static function processCardTag($input, array $args, Parser $parser, PPFrame $frame) {
		if (!empty($args['title'])) {
			$card  = $args['title'];
			$title = $input;
		} else {
			$title = '';
			$card  = $input;
		}
		return self::mtgsBbcodeCardHandle($card, $title, ' style="color: #804000;font-weight: bold;"');
	}

	/**
	 * Processes Multiple Card Tags
	 *
	 * @access public
	 * @param  string	Content between opening and closing tags.
	 * @param  array	Arguments on the opening tag.
	 * @param  object	Mediawiki Parser
	 * @param  object	Mediawiki PPFrame
	 * @return string	HTML
	 */
	public static function processCardsTag($input, array $args, Parser $parser, PPFrame $frame) {
		$cardlist = self::mtgsBbcodeCardsHandle($input, ' style="font-weight: bold;color: #286014;"');
		return $cardlist;
	}

	/**
	 * Processes Deck Tags
	 *
	 * @access public
	 * @param  string	Content between opening and closing tags.
	 * @param  array	Arguments on the opening tag.
	 * @param  object	Mediawiki Parser
	 * @param  object	Mediawiki PPFrame
	 * @return string	HTML
	 */
	public static function processDeckTag($input, array $args, Parser $parser, PPFrame $frame) {
		if (!empty($args['title'])) {
			$title = $args['title'];
		} else {
			$title = '';
		}
		return self::mtgsBbcodeDeckHandle('wikiid', $parser->mTitle->getArticleId(), $input, $title, '  style="font-weight: bold;color: #286014;"');
	}
}
