<?php
/**
 * MTGSCards
 * MTGSCards Mediawiki Settings
 **/

if (function_exists('wfLoadExtension')) {
	wfLoadExtension('MTGSCards');
	wfWarn(
		'Deprecated PHP entry point used for MTGSCards extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
} else {
	die('This version of the MTGSCards extension requires MediaWiki 1.25+');
}
