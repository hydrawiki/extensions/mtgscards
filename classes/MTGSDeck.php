<?php
/**
 * MTGSCards
 * MTGSCards Deck based on deck.php from forums-mtgsalvation
 *
 * @license Creative Commons Attribution Share Alike 2.5 Generic
 * @author  Stephan Hoyer ("Darth Cow") - https://www.sccs.swarthmore.edu/users/08/hoyer/decktag/
 * @author  Further maintenance by the Hydra Wiki Platform Team for the MTGS Wiki.
 **/

class MTGSDeck {
	static function handle_deck_text($deck, $name) {
		return "$name from MTGSalvation.com\n$deck";
	}

	static function make_deck() {
		if ($_REQUEST['source'] == 'wikiid' && intval($_REQUEST['id']) > 0) {
			$wiki = true;
			$username = 'MTGS Wiki';
			$page = WikiPage::newFromId(intval($_REQUEST['id']));
			$text = $page->getContent()->getNativeData();
		} else {
			die('no deck');
		}

		$preg_match = "/(\<)(deck|d)( title(=)(&quot;|\"|\'|)([^\]]+)(&quot;|\"|\'|))?\>(.+)(\<\/(deck|d)\>)/siU";

		preg_match_all($preg_match, $text, $decks, PREG_SET_ORDER);

		$num = $_GET['num'];
		$format = $_GET['format'];

		if ($format == "debug") {
			echo "<pre>";
			print_r($decks);
			echo "</pre>";
			exit;
		}

		if (!array_key_exists($num, $decks) or !in_array($format, ['magiconline', 'magicworkstation', 'apprentice', 'octgn', 'text', 'debug'])) {
			$idname = "Deck";
			echo 'invalid format';
			exit;
		}

		$name = $decks[$num][6];
		$deck = $decks[$num][8];

		if ($name == "") {
			$name = str_replace('"', "", $username) . "'s deck";
		}
		$filename = str_replace(["\n", "\r\n", "\r", '.', ':', '/', '\\', '<', '>', '"', '|', '?', '*', "\0", ';'], "", $name);

		// format the deck nicely
		$deck = preg_replace("/[\040\011]*(\r\n|\r|\n)[\040\011]*/", "\n", $deck);
		$deck = trim($deck);

		// ############################ Apprentice ##############################

		if ($format == "apprentice") {
			header("Content-type: application/dec");
			header("Content-Disposition: attachment; filename=\"$filename.dec\"");

			echo self::handle_deck_apprentice($deck, $name);
		}

		// ########################## Magic Online ##############################

		if ($format == "magiconline") {
			header("Content-type: application/txt");
			header("Content-Disposition: attachment; filename=\"$filename.txt\"");

			echo self::handle_deck_magiconline($deck);
		}

		// ############################## OCTGN #################################

		if ($format == "octgn") {
			header("Content-type: text/plain");
			header("Content-Disposition: attachment; filename=\"$filename.o8d\"");

			echo self::handle_deck_octgn2($deck, $name, $post['username']);
		}

		// ############################### Text #################################

		if ($format == "text") {
			// header("Content-type: application/txt");
			// header("Content-Disposition: attachment; filename=\"$$filename.txt\"");

			echo self::handle_deck_text($deck, $name);
		}
	}

	// Returns the contents of a deck in the Apprentice file format
	static function handle_deck_apprentice($deck, $name, $setinfo = false) {
		$formatted_deck = "// Name: $name from MTGSalvation.com\n";

		$sections = preg_split("/\n{2,}/", $deck);

		foreach ($sections as $section) {
			// If this section is the sideboard, format card lines appropriately
			if (preg_match("/side|board|sb/i", $section)) {
				$section = preg_replace("/SB\:/i", "", $section);
				$sb = 'SB:  ';
			} else {
				$sb = '        ';
			}

			// Comment all lines
			$section = preg_replace("/^\s*(.*)/m", "// $1", $section);
			// Uncomment and format all lines with cards
			$cardword = "\w+[',:!\-]*\w*";

			if ($setinfo) {
				$section = preg_replace(
					"#^//\s*(\d+)x?\040*(\040\[\w{2,3}\])?\040*($cardword(\040$cardword)*).*#m",
					"$sb$1$2 $3",
					$section
				);
			} else {
				$section = preg_replace(
					"#^//\s*(\d+)x?\040*(\040\[\w{2,3}\])?\040*($cardword(\040$cardword)*).*#m",
					"$sb$1 $3",
					$section
				);
			}

			$formatted_deck .= $section . "\n";
		}

		$formatted_deck = trim(str_replace("\n", "\r\n", $formatted_deck));

		return $formatted_deck;
	}

	// Return a deck as a Magic Online text file
	static function handle_deck_magiconline($deck) {
		$deck = trim($deck);
		$sections = preg_split("/\n{2,}/", $deck);

		foreach ($sections as $section) {
			// If this section is the sideboard, title it Sideboard
			if (preg_match("/side|board|sb/i", $section)) {
				$section = preg_replace("/SB\:/i", "", $section);
				$sbline = "Sideboard\n";
			} else {
				$sbline = "";
			}

			// Format cards
			$cardword = "\w+[',:!\-]*\w*";
			$section = preg_replace("#^\s*(\d+)x?\040*($cardword(\040$cardword)*).*#m", "$1 $2", $section);

			// Remove any extra lines
			$section = preg_replace("/^[^\d].*\n?/m", "", $section);

			$formatted_deck .= $sbline . $section . "\n";
		}

		$formatted_deck = trim(str_replace("\n", "\r\n", $formatted_deck));

		return $formatted_deck;
	}

	// Returns the contents of a deck in the OCTGN file format
	static function handle_deck_octgn($deck, $name, $author) {
		$downloadedfrom = str_replace('&', '&amp;', $_SERVER['REQUEST_URI']);
		$formatted_deck = "<?xml version=\"1.0\"?>\n"
			. "<wizardsmagicdeck Version=\"1.0\"><header><charset codepage=\"1252\"/></header>"
			. "<deck><deckinfo><title>$name</title>"
			. "<decktypeid>0</decktypeid><creator>$author</creator>"
			. "<version></version><description>Deck downloaded from http://old-forums.mtgsalvation.com$downloadedfrom</description>"
			. "<email></email><comments>Deck downloaded from http://old-forums.mtgsalvation.com$downloadedfrom</comments></deckinfo><maindeck>";

		$entities = preg_split("/\n{1,}/", $deck);

		// NOTE: not my code
		$sb = 0;
		for ($i = 0; $i < count($entities); $i++) { // loop through the entities
			if (strlen($entities[$i]) > 1) { // only bother with entities longer than 1 character to avoid random line breaks
				if ($sb == 0 && strtolower($entities[$i]) === "sideboard") { // switch to writing sideboard
					$formatted_deck .= "</maindeck><sideboard>"; // the changeover
					$sb = 1; // stop the script repeating this in case a user specifies a second sb
				} // end if for sideboard switch

				if (preg_match("/[0-9]+(.[a-zA-Z ])/Uis", $entities[$i])) { // this matches a card line
					$cardline = explode(" ", $entities[$i], 2); // explode line into name and qty

					$num_cards = $cardline[0]; // qty of card
					$name_card = $cardline[1]; // name of card

					for ($j = 0; $j < $num_cards; $j++) { // a number of times equal to qty
						$formatted_deck .= "<card id=\"0\">$name_card</card>"; // write the card
					} // end for for qty
				} // end if/else for comment or card
			} // end if for string length
		} // end for for looping thru deck
		$formatted_deck .= ($sb == 1 ? "</sideboard></deck></wizardsmagicdeck>" : "</maindeck><sideboard/></deck></wizardsmagicdeck>"); // final tags for with/without sb
		// end not my code

		$formatted_deck = trim(str_replace("\n", "\r\n", $formatted_deck));

		return $formatted_deck;
	}

	static function handle_deck_octgn2($deck, $name, $author) {
		$downloadedfrom = str_replace('&', '&amp;', $_SERVER['REQUEST_URI']);
		$formatted_deck = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>\n<deck game=\"a6c8d2e8-7cd8-11dd-8f94-e62b56d89593\">\n<section name=\"Main\">";

		$entities = preg_split("/\n{1,}/", $deck);

		// NOTE: not my code
		$sb = 0;
		for ($i = 0; $i < count($entities); $i++) { // loop through the entities
			if (strlen($entities[$i]) > 1) { // only bother with entities longer than 1 character to avoid random line breaks
				if ($sb == 0 && strtolower($entities[$i]) === "sideboard") { // switch to writing sideboard
					$formatted_deck .= "</section><section name=\"Sideboard\">"; // the changeover
					$sb = 1; // stop the script repeating this in case a user specifies a second sb
				} // end if for sideboard switch

				if (preg_match("/[0-9]+(.[a-zA-Z ])/Uis", $entities[$i])) { // this matches a card line
					$cardline = explode(" ", $entities[$i], 2); // explode line into name and qty
					$num_cards = intval($cardline[0]); // qty of card
					$name_card = $cardline[1]; // name of card
					$formatted_deck .= "<card qty=\"$num_cards\" id=\"88e8742b-5da7-4458-853f-0fd10980d959\">$name_card</card>";
				} // end if/else for comment or card
			} // end if for string length
		} // end for for looping thru deck

		$formatted_deck .= ($sb == 1 ? "</section><section name=\"Command Zone\" /></deck>" : "</section><section name=\"Sideboard\" /><section name=\"Command Zone\" /></deck>"); // final tags for with/without sb
		// end not my code

		$formatted_deck = trim(str_replace("\n", "\r\n", $formatted_deck));

		return $formatted_deck;
	}
}
