<?php
/**
 * MTGSCards
 * Special page that serves a download of a deck in various formats
 *
 * @author  Noah Manneschmidt
 * @package MTGSCards
 **/

class SpecialDownloadDeck extends \UnlistedSpecialPage {
	public function __construct() {
		parent::__construct('DownloadDeck');
	}

	/**
	 * Show the special page
	 *
	 * @param $params Mixed: parameter(s) passed to the page or null
	 */
	public function execute($subpage) {
		$wgOut = $this->getOutput();
		$wgOut->disable();
		MTGSDeck::make_deck();
	}
}
